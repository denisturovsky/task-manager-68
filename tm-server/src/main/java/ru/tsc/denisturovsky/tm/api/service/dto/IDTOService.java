package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.AbstractModelDTO;
import ru.tsc.denisturovsky.tm.enumerated.CustomSort;

import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<M> findAll(@Nullable CustomSort sort) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    int getSize() throws Exception;

    void remove(@Nullable M model) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    void update(@Nullable M model) throws Exception;

}
