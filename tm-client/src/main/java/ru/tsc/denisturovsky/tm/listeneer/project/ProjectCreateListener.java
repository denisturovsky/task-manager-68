package ru.tsc.denisturovsky.tm.listeneer.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.request.ProjectCreateRequest;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Create new project";

    @NotNull
    public static final String NAME = "project-create";

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @NotNull final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END:");
        @NotNull final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        request.setDateBegin(dateBegin);
        request.setDateEnd(dateEnd);
        projectEndpoint.createProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
