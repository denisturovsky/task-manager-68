package ru.tsc.denisturovsky.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDTO", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "dateBegin",
        "dateEnd"
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private String name = "";

    @Column
    @NotNull
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    @XmlSchemaType(name = "string")
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    @XmlElement(required = true, namespace = "http://denisturovsky.tsc.ru/tm/dto/soap")
    private Date dateEnd;

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
        this.description = description;
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        return String.format(
                "%30s:%30s:%30s:%30s:%30s|",
                name,
                getStatus().getDisplayName(),
                description,
                DateUtil.toString(getDateBegin()),
                DateUtil.toString(getDateEnd())
        );
    }

}

