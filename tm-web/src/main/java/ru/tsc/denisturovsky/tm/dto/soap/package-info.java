@javax.xml.bind.annotation.XmlSchema(
        namespace = "http://denisturovsky.tsc.ru/tm/dto/soap",
        elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package ru.tsc.denisturovsky.tm.dto.soap;