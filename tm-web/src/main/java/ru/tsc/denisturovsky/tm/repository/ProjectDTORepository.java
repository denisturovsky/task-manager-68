package ru.tsc.denisturovsky.tm.repository;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

}
