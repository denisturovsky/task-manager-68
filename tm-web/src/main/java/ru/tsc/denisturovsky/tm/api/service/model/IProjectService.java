package ru.tsc.denisturovsky.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@NotNull Project model) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    int count() throws Exception;

    @NotNull
    Project create(@Nullable String name) throws Exception;

    @NotNull
    Project create(
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOneById(@Nullable String id) throws Exception;

    void remove(@Nullable Project model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void update(@Nullable Project model) throws Exception;

    void updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}